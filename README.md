# Gradle Background Execute & Selenium Plugins

These plugins allow you to quickly and easily run background tasks and spin up
Selenium server instances.

Usage of the plugins:
- [Background Execute](README_SPAWN.md)
- [Selenium Plugin](README_SELENIUM.md)