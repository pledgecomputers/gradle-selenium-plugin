# Gradle Selenium Plugin
This plugin helps out when it comes to using Selenium with Gradle-based projects.

The Selenium Gradle plugin uses this to start and stop a Selenium server
instance.

## Setup
Follow the [instructions on the Gradle Plugin page for this plugin][gradle-plugin-page].

## Configuration
This plugin adds a `selenium {}` extension to your project.  Here's a full configuration, 
with the defaults specified:
```groovy
selenium {

    // The base url to use for the Selenium WebDriver wrapper.
    // Note that this is mainly helpful if you also use the 
    // `gradle-selenium-helper` library in your tests.
    siteBaseUrl = null

    // This is where the default tasks download the prerequisites by default.
    seleniumDir = new File(project.buildDir, 'selenium')
    seleniumServerJar = new File(seleniumDir, 'selenium-standalone-server.jar')
    seleniumIEDriverExe = new File(seleniumDir, 'selenium')
    seleniumChromeDriverExe = new File(project.buildDir, 'selenium')

    // Specify the hub information.
    hub {
        // See the Hubs section below.
    }

    // Specify a browser to run with.  This closure can be repeated multiple times
    // for each desired browser.
    browser {
        // See the Browsers section below.
    }

    // Alternatively, you can specify nodes to run with.  This can be repeated multiple
    // times for each node you want to run.
    browser {
        // See the Nodes section below.
    }

}
```

### Hubs
You can specify either a local or a remote hub (not both!).  
To specify a local hub, use
```groovy
selenium {
    ...
    hub {
        // You can either specify these... (default values shown for a local)
        port = 4444
        host = 'localhost'
        webDriverUrl = "http://$host:$port/wd/hub"

        // Or you can specify this.  If you specify both, this will take
        // precedence.  In this configuration, this will take precedence.
        hubJson = project.file('hub.json')

        // You can also, optionally, specify additional arguments for the command:
        args = ['-DPOOL_MAX=512']
    }
    ...
}
```
To specify that this is to be run with a remote hub, set `useRemote` to false on the 
`selenium {}` extension and specify a `hub {}` configuration:
```groovy
selenium {
    ...
    useRemote = true
    hub {
        ...
    }
    ...
}
```
Alternatively, you can just use a `remote {}` closure as a shortcut:
```groovy
selenium {
    ...
    remote {
        // This takes the same options as the `hub {}` closure.
    }
    ...
}
```
### Nodes
Nodes are defined on the `selenium {}` extension.  It can be specified multiple times
for multiple nodes.  You can add any of the [Selenium Node options][selenium-node-conf]:
```groovy
selenium {
    ...
    node {
        // You can either specify these... (default values shown for a local)
        isLocal = true
        host = 'localhost'
        port = 4444
        timeout = 300
        browserTimeout = 60
        maxSession = 5

        // You can then specify `browser {}` configurations.  This can be repeated
        // multiple times, once for each browser the node should handle.
        browser {
            ...
        }

        // Or you can specify this.  If you specify both, this will take
        // precedence.  In this configuration, this will take precedence.
        nodeJson = project.file('node1.json')

        // You can also, optionally, specify additional arguments for the command.
        // The default is `[]`.
        args = ['-DPOOL_MAX=512']

        // Finally, you can specify extra properties which will be passed
        // along to your tests, if you use the gradle-selenium-helper library.
        properties = ['useRed':false]
    }
    ...
}
```

### Browsers
Browsers are defined on either the `selenium {}` extension or on each `node {}` extension.
You can add any of the [Selenium node `-browser` options][selenium-node-conf]:
```groovy
...
browser {
    browserName = "chrome"
    binary = "/path/to/chrome/executable"
    maxInstances = 5
    platform = "LINUX"

    // You can also, optionally, specify additional arguments for the command.
    // The default is an empty string.
    args = 'customArg1=Zoidberg,customArg2=3235'

    // Finally, you can specify extra properties which will be passed
    // along to your tests, if you use the gradle-selenium-helper library.
    properties = ['useRed':false]

}
...
```

## Adding Selenium Test Tasks
Once the `selenium {}` extension has been configured appropriately,
you just need to add some Selenium Test tasks:
```groovy
import com.pledgecomputers.gradle.selenium.SeleniumTest

task seleniumTests(type: SeleniumTest) {
    // Here you can configure the task just like a Test task.
}

```

## Built-in Tasks
Finally, this plugin adds a number of tasks for you:
- `downloadIEDriverZip` - Downloads the Selenium IE Driver .zip file.  (Windows only,
  obviously)
- `downloadAndUnzipIEDriver` - Downloads and unzips the Selenium IE Driver.  (Windows only,
  obviously)
- `downloadChromeDriverZip` - Downloads the Selenium Chrome Driver .zip file.
- `downloadAndUnzipChromeDriver` - Downloads and unzips the Selenium Chrome Driver.
- `downloadSeleniumServerJar` - Downloads the Selenium Standalone Server.
- `initSeleniumEnv` - Downloads and extracts the Selenium Standalone Server and 
  all applicable drivers.




[gradle-plugin-page]: https://plugins.gradle.org/plugin/com.pledgecomputers.gradle.selenium
[selenium-node-conf]: https://github.com/SeleniumHQ/selenium/wiki/Grid2#configuring-the-nodes-by-command-line