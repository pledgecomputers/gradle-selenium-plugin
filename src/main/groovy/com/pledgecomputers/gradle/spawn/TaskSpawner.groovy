package com.pledgecomputers.gradle.spawn

import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

import groovy.lang.Closure;

class TaskSpawner  implements Spawnable {
    private Logger logger = Logging.getLogger(TaskSpawner.class)

    Task start
    Task end

    private Project project

    TaskSpawner(Project project){
        this.project = project
    }
    
    @Override
    public void spawn() {
        if(start!=null)
            start.actions.each {act->
                act.execute(start)
            }
    }

    @Override
    public void kill() {
        if(end!=null)
            end.actions.each {act->
                act.execute(end)
            }
    }

}
