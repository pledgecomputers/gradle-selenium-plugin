
package com.pledgecomputers.gradle.spawn

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging


class ProcessSpawner implements Spawnable {
    private Logger logger = Logging.getLogger(ProcessSpawner.class)
    private Project project
    private Process process
    
    List<String> command
    String ready
    File directory
    
    
    ProcessSpawner(Project project){
        this.project = project
        this.directory = project.projectDir
    }
    
    void spawn() {
        logger.lifecycle "spawn()"
        if (!(command && ready)) {
            throw new GradleException("Ensure that mandatory fields command and ready are set.")
        }

        buildProcess()
        waitFor()
        checkForAbnormalExit()
    }
    
    void kill() {
        process.destroy()
    }

    private void checkForAbnormalExit() {
        try {
            if (process.exitValue()) {
                throw new GradleException("The process terminated unexpectedly - status code ${process.exitValue()}")
            }
        } catch (IllegalThreadStateException ignored) {}
    }

    private waitFor() {
        def line
        def reader = new BufferedReader(new InputStreamReader(process.getInputStream()))

        while ((line = reader.readLine()) != null) {
            logger.lifecycle line
            if (line.contains(ready)) {
                logger.quiet "$command is ready."
                break;
            }
        }
    }

    private buildProcess() {
        logger.lifecycle String.format("Executing: %s",command)
        def builder = new ProcessBuilder(command as String[])
        builder.redirectErrorStream(true)
        builder.directory(directory)
        process = builder.start()
    }
    
    @Override
    public String toString(){
        return String.format("ProcessSpawner(\n  command=\"%s\",\n  ready=\"%s\",\n  directory=\"%s\")",command, ready, directory)  
    }
}