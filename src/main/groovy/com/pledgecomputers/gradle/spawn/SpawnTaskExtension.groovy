package com.pledgecomputers.gradle.spawn

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

class SpawnTaskExtension {
    private Project project;
    private Task task;

    def spawns =  []
    
    SpawnTaskExtension(Project project, Task task){
        this.project = project;
        this.task = task;
        this.spawns = new ArrayList<Spawnable>();
    }

    def spawn(Closure closure) {
        closure.delegate = this
        closure()
    }

    def process(Closure closure) {
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        ProcessSpawner spawner = new ProcessSpawner(project)
        closure.delegate = spawner
        spawns << spawner
        closure()
    }
    
    def closures(Closure closure) {
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        ProcessSpawner spawner = new ClosureSpawner(project)
        closure.delegate = spawner
        spawns << spawner
        closure()
    }
    
    def tasks(Closure closure) {
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        ProcessSpawner spawner = new TaskSpawner(project)
        closure.delegate = spawner
        spawns << spawner
        closure()
    }
    
    void setupHooks() {
        task.doFirst { t->
            spawn.start()
        }
        
        task.doLast { t->
            spawn.end()
        }
    }
    
    void doBefore() {
        task.spawn.start()
    }
    
    void doAfter() {
        task.spawn.end()
    }
    
    void start() {
        spawns.each {s-> 
            s.spawn()
        }
    }
    
    void end() {
        spawns.each {s->
            s.kill()
        }
    }
}