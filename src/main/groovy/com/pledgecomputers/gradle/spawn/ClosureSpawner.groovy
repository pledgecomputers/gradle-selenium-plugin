package com.pledgecomputers.gradle.spawn

import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

class ClosureSpawner implements Spawnable {
    private Logger logger = Logging.getLogger(ProcessSpawner.class)

    Closure start
    Closure end

    private Project project

    ClosureSpawner(Project project){
        this.project = project
        this.start = null
        this.end = null
    }

    public void start(Closure c) {
        this.start = c;
    }

    public void end(Closure c) {
        this.end = c;
    }
    
    @Override
    public void spawn() {
        if(start!=null) {
            start.resolveStrategy = Closure.DELEGATE_FIRST
            start.delegate = project
            start()
        }
    }

    @Override
    public void kill() {
        if(end!=null){
            end.resolveStrategy = Closure.DELEGATE_FIRST
            end.delegate = project
            end()
        }
    }

}
