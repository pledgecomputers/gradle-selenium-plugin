
package com.pledgecomputers.gradle.spawn

interface Spawnable {
    void spawn();
    void kill();
}