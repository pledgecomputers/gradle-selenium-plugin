package com.pledgecomputers.gradle.spawn

import org.gradle.api.Plugin
import org.gradle.api.Project

class SpawnPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.tasks.all {task -> 
            task.extensions.create("spawn", SpawnTaskExtension, project, task)
        }
        
        project.getGradle().getTaskGraph().beforeTask {task ->
            task.spawn.doBefore()
        }
        
        project.getGradle().getTaskGraph().afterTask {task ->
            task.spawn.doAfter()
        }
    }
}