package com.pledgecomputers.gradle.selenium

import org.gradle.api.Plugin
import org.gradle.api.Project

import com.pledgecomputers.gradle.spawn.SpawnPlugin

class SeleniumBasePlugin implements Plugin<Project> {
    static final String CONFIGURATION_NAME = 'selenium'
    static final String SELENIUM_MAVEN_GROUP = 'org.seleniumhq.selenium'
    static final String SELENIUM_MAVEN_ARTIFACT_JAVA = 'selenium-java'
    static final String SELENIUM_JAVA_DEFAULT_VERSION = '2.53.0'

    @Override
    void apply(Project project) {
        project.plugins.apply(SpawnPlugin)
        SeleniumProjectExtension selExt = project.extensions.create("selenium", SeleniumProjectExtension, project)
        
        addConfiguration(project)

        configureAbstractContainerTask(project, selExt)
        
    }
    
    private void addConfiguration(Project project) {
        /*
        project.configurations.create(CONFIGURATION_NAME)
            .setVisible(false)
            .setTransitive(true)
            .setDescription('The Selenium Tests\' libraries to be used for this project.')
        */
        
    }

    private void configureAbstractContainerTask(Project project, SeleniumProjectExtension selExt) {
        project.afterEvaluate {
            selExt.afterEvaluate()
            
            project.tasks.withType(SeleniumTest) {
                /*
                conventionMapping.map('classpath') {
                    def config = project.configurations[CONFIGURATION_NAME]
    
                    if(config.dependencies.empty) {
                        project.dependencies {
                            selenium "$SELENIUM_MAVEN_GROUP:$SELENIUM_MAVEN_ARTIFACT_JAVA:${selExt.seleniumJavaVersion}"
                        }
                    }
    
                    config
                }
                */
                
                configure({
                    
                    project.selenium.getSystemProperties().each {k,v->
                        systemProperty(k,v)
                    }
                    
                    spawn { s->

                        selExt.getCommands().each {cmd->
                            s.process {
                                command = cmd.getCommand()
                                ready = cmd.getReady()
                            }
                        }
                    }
                    
                })
            }
        }
        
    }
}
