package com.pledgecomputers.gradle.selenium

interface Commandable {
    List<String> getCommand();
    String getReady();
}
