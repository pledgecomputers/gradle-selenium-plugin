package com.pledgecomputers.gradle.selenium

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.internal.os.OperatingSystem

import de.undercouch.gradle.tasks.download.Download

class SeleniumPlugin extends SeleniumBasePlugin {
    
    static final String SELENIUM_IE_DRIVER_KEY = '2.49/IEDriverServer_Win32_2.49.0.zip'
    static final String SELENIUM_CHROME_DRIVER_VERSION = '2.20'
    static final String SELENIUM_SERVER_STANDALONE_KEY = '2.49/selenium-server-standalone-2.49.0.jar'
    
    void apply(Project project) {
        super.apply(project)
        SeleniumProjectExtension selExt = project.selenium
        addDownloadTasks(project, selExt)
    }
    
    void addDownloadTasks(Project project, SeleniumProjectExtension selExt) {
        def seleniumDir = selExt.seleniumDir
        def seleniumServerJar = selExt.seleniumServerJar
        
        if(OperatingSystem.current().isWindows()) {
            def downloadIEDriverZip = project.task('downloadIEDriverZip', type: Download, group: 'Selenium') {
                src "http://selenium-release.storage.googleapis.com/${selExt.ieDriverKey}"
                dest new File(seleniumDir, 'IEDriverServer.zip')
                quiet false
                overwrite true
                onlyIfNewer true
                compress false
            
                doFirst {
                    println "Download IE driver: " + src+ " to " + dest
                }
            }
        
            project.task('downloadAndUnzipIEDriver', dependsOn: downloadIEDriverZip, type: Copy, group: 'Selenium') {
              
                from project.zipTree(downloadIEDriverZip.dest)
                into seleniumDir
              
                doFirst {
                  println "Unzip IE driver: " + downloadIEDriverZip.dest
                }
            }
        }
        
        def downloadChromeDriverZip = project.task('downloadChromeDriverZip', type: Download, group: 'Selenium') {
            if (OperatingSystem.current().isMacOsX()) {
                src "http://chromedriver.storage.googleapis.com/${selExt.chromeDriverVersion}/chromedriver_mac32.zip"
            }
            else if (OperatingSystem.current().isLinux()) {
                src "http://chromedriver.storage.googleapis.com/${selExt.chromeDriverVersion}/chromedriver_linux64.zip"
            }
            else {
                src "http://chromedriver.storage.googleapis.com/${selExt.chromeDriverVersion}/chromedriver_win32.zip"
            }
            dest new File(seleniumDir, 'chromedriver.zip')
            quiet false
            overwrite true
            onlyIfNewer true
            compress false
          
            doFirst {
                println "Download Chrome driver: " + src + " to " + dest
            }
        }
        
        project.task('downloadAndUnzipChromeDriver', dependsOn: project.tasks.downloadChromeDriverZip, type: Copy, group: 'Selenium') {
            doFirst {
                println "Unzip Chrome driver: " + downloadChromeDriverZip.dest
            }
            from project.zipTree(downloadChromeDriverZip.dest)
            into seleniumDir
        }
        
        project.task('downloadSeleniumServerJar', type: Download, group: 'Selenium') {
            src "http://selenium-release.storage.googleapis.com/${selExt.seleniumServerStandaloneKey}"
            dest seleniumServerJar
            quiet false
            overwrite true
            onlyIfNewer true
            compress false
          
            doFirst {
                println "Download Selenium Server Standalone: " + src + " to " + dest
            }
        }
        if(OperatingSystem.current().isWindows())
            project.task('initSeleniumEnv', dependsOn: ['downloadSeleniumServerJar', 'downloadAndUnzipChromeDriver', 'downloadAndUnzipIEDriver'], group: 'Selenium')
        else
            project.task('initSeleniumEnv', dependsOn: ['downloadSeleniumServerJar', 'downloadAndUnzipChromeDriver'], group: 'Selenium')
    }

}