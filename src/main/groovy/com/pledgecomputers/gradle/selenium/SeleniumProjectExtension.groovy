package com.pledgecomputers.gradle.selenium

import java.util.Map;

import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

class SeleniumProjectExtension {
    Logger slf4jLogger = Logging.getLogger(SeleniumProjectExtension)
    
    private Project project;
    private SeleniumNode defaultNode
    
    Boolean useRemote = false
    
    File seleniumDir
    File seleniumServerJar
    File seleniumIEDriverExe
    File seleniumChromeDriverExe
    
    SeleniumHub hub
    
    List<SeleniumNode> nodes = []
    
    String siteBaseUrl
    
    String propertyBase = 'selenium'
    
    String ieDriverKey = '2.53/IEDriverServer_Win32_2.53.0.zip'
    String chromeDriverVersion = '2.21'
    String seleniumServerStandaloneKey = '2.53/selenium-server-standalone-2.53.0.jar'
    String seleniumJavaVersion = '2.53.0'
    
    SeleniumProjectExtension(Project project){
        this.project = project;
        this.seleniumDir = new File(project.buildDir, 'selenium')
        this.seleniumServerJar = new File(seleniumDir, 'selenium-server-standalone.jar')
        this.seleniumIEDriverExe = new File(seleniumDir, 'IEDriverServer.exe')
        this.seleniumChromeDriverExe = new File(seleniumDir, 'chromedriver.exe')
        this.hub = new SeleniumHub(this)
        this.defaultNode = new SeleniumNode(this, 0);
    }
    
    def selenium(Closure closure){
        closure.delegate = this
        closure()
    }
    
    def hub(Closure closure) {
        useRemote = false
        
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure.delegate = hub
        closure()
    }
    
    def remote(Closure closure) {
        useRemote = true
        
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        closure.delegate = hub
        closure()
    }
    
    def node(Closure closure) {
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        SeleniumNode node = new SeleniumNode(this, nodes.size())
        closure.delegate = node
        nodes << node
        closure()
    }
    
    def browser(Closure closure) {
        defaultNode.browser(closure)
    }
    
    void afterEvaluate() {
        def self = this
        if(nodes.size()==0){
            nodes << defaultNode
        }
    }
    
    List<Commandable> getCommands() {
        if(useRemote)
            return [];
        
        if(nodes.size()>1)
            return [hub]+nodes
        else
            return [hub.getLocalCommand(nodes)]
        
    }
    
    Map<String, String> getSystemProperties() {
        Map<String, String> props = [:]
        String propBase = propertyBase
        
        if(siteBaseUrl!=null)
            props.put("${propBase}.site.baseUrl", siteBaseUrl)
        
        props += hub.getSystemProperties(propBase)
        
        nodes.each {node->
            props += node.getSystemProperties(propBase)
        }
        
        return props
    }
}