package com.pledgecomputers.gradle.selenium

import java.util.List;

class SeleniumNode implements Commandable {
    
    private SeleniumProjectExtension selExt
    private int id
    
    Boolean isLocal = Boolean.TRUE
    
    File nodeJson = null
    
    String host
    Integer port
    Integer timeout
    String browserTimeout
    Integer maxSession
    
    List<String> args
    
    List<SeleniumBrowser> browsers = []
    
    Map<String, String> properties = [:]
    
    SeleniumNode(SeleniumProjectExtension selExt, int id){
        this.selExt = selExt 
        this.id = id
    }
    
    def browser(Closure closure) {
        closure.resolveStrategy = Closure.DELEGATE_FIRST
        SeleniumBrowser browser = new SeleniumBrowser(browsers.size())
        closure.delegate = browser
        browsers << browser
        closure()
    }
    
    void afterEvaluate() {
        
    }

    @Override
    public List<String> getCommand() {
        
        def cmd = ['java', '-jar', selExt.seleniumServerJar, '-role', 'node', "-Dwebdriver.chrome.driver=${selExt.seleniumChromeDriverExe}", "-Dwebdriver.ie.driver=${selExt.seleniumIEDriverExe}"]
        
        if(nodeJson!=null) {
            cmd += ['-nodeConfig', nodeJson.toString()]
        } else {
            if(host!=null)
                cmd += ['-host', host]
            if(port!=null)
                cmd += ['-port', port]
            if(timeout!=null)
                cmd += ['-timeout', timeout]
            if(browserTimeout!=null)
                cmd += ['-browserTimeout', browserTimeout]
            if(maxSession!=null)
                cmd += ['-maxSession', maxSession]
            if(args!=null)
                cmd += args
                
            browsers.each {b -> 
                def b_cmd = b.getCommand()
                if(b_cmd!=null)
                    cmd += b_cmd
            }
        }
        
        return cmd
    }

    @Override
    public String getReady() {
        return 'The node is registered to the hub and ready to use';
    }
    
    Map<String, String> getSystemProperties(String propBase){
        Map<String, String> props = [:]
        propBase += ".node$id"
        
        if(nodeJson!=null) {
            props.put("${propBase}.nodeConfig", nodeJson.toString())
        } else {
            if(host!=null)
                props.put("${propBase}.host", host)
            if(port!=null)
                props.put("${propBase}.port", port)
            if(timeout!=null)
                props.put("${propBase}.timeout", timeout)
            if(browserTimeout!=null)
                props.put("${propBase}.browserTimeout", browserTimeout)
            if(maxSession!=null)
                props.put("${propBase}.maxSession", maxSession)
                
        }
        
        browsers.each {b ->
            def b_props = b.getSystemProperties(propBase)
            if(b_props!=null)
                props += b_props
        }
        
        properties.each {k,v->
            props.put("${propBase}.$k", v)
        }
        
        return props
    }
    
    String toString() {
        return String.format("SeleniumNode(%s)", getCommand())
    }

}
