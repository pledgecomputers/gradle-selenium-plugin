
package com.pledgecomputers.gradle.selenium

import java.util.List;
import java.util.Map;

class SeleniumBrowser {
    private int id
    String browserName
    
    File binary
    
    Integer maxInstances
    
    String platform
    
    String args = ""
    
    Map<String, String> properties = [:]
    
    SeleniumBrowser(int id) {
        this.id = id
    }
    
    String toString() {
        return String.format("SeleniumBrowser(browserName: %s, binary: %s, maxInstances: %s, platform: %s, map: %s)",browserName, binary, maxInstances, platform, args)
    }
    
    def getCommand() {
        def cmd = ''
        
        if(browserName!=null)
            cmd += (cmd.length()>0?',':'')+'browserName='+browserName
        
        if(binary && (browserName=='chrome' || browserName=='firefox')){
            cmd += (cmd.length()>0?',':'')+ browserName + '_binary=' + binary.toString()
        }
        
        if(maxInstances!=null)
            cmd += (cmd.length()>0?',':'')+'maxInstances='+maxInstances
            
        if(platform!=null)
            cmd += (cmd.length()>0?',':'')+ 'platform='+platform
        
        if(args.length()>0)
            cmd += (cmd.length()>0?',':'')+ args
        
        if(cmd.length()>0){
            return ['-browser', cmd]
        }
        return null
    }
    
    Map<String, String> getSystemProperties(String propBase) {
        propBase+=".browser$id"
        Map<String, String> props = [:]
        
        if(browserName!=null)
            props.put("${propBase}.browserName", browserName)
        if(maxInstances!=null)
            props.put("${propBase}.maxInstances", maxInstances)
        if(platform!=null)
            props.put("${propBase}.platform", platform)
        
        
        properties.each {k,v->
            props.put("${propBase}.$k", v)
        }
        
        return props
    }
}