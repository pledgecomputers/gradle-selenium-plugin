package com.pledgecomputers.gradle.selenium

import java.io.File
import java.util.List;

class SeleniumHub implements Commandable {
    
    private SeleniumProjectExtension selExt

    private boolean isLocal = false
    private List<SeleniumNode> nodes
    
    File hubJson
    
    Integer port = 4444
    String host
    String webDriverUrl
    
    List<String> args = []
    
    SeleniumHub(SeleniumProjectExtension selExt){
        this.selExt = selExt
    }
    
    @Override
    List<String> getCommand() {
        if(isLocal) {
            def cmd = ['java', '-jar', selExt.seleniumServerJar, "-Dwebdriver.chrome.driver=${selExt.seleniumChromeDriverExe}", "-Dwebdriver.ie.driver=${selExt.seleniumIEDriverExe}"]
        
            for(SeleniumNode node : nodes){
                for(SeleniumBrowser b:node.browsers){
                    def b_cmd = b.getCommand()
                    if(b_cmd!=null)
                        cmd += b_cmd
                }
            }
            return cmd
        } else {
            
            def cmd = ['java', '-jar', selExt.seleniumServerJar, '-role', 'hub']
            
            if(hubJson) {
                cmd += ['-hubConfig', hubJson]
            } else {
                if(host!=null)
                    cmd += ['-host', host]
                if(port!=null)
                    cmd += ['-port', port]
            }
            
            return cmd+args
        }
    }
    
    SeleniumHub getLocalCommand(List<SeleniumNode> nodes) {
        this.isLocal = true

        this.nodes = nodes
        
        return this
    }
    
    @Override
    public String getReady() {
        return 'Selenium Server is up and running';
    }
    
    String toString() {
        return String.format("SeleniumHub(%s)", getCommand())
    }
    
    String getWebDriverUrl(){
        if(webDriverUrl!=null)
            return webDriverUrl
        
        if(host!=null)
            return "http://$host:$port/wd/hub"
        
        return "http://localhost:$port/wd/hub"
    }
    
    Map<String, String> getSystemProperties(String propBase) {
        propBase += '.hub'
        return [
            "${propBase}.host": host,
            "${propBase}.port": port,
            "${propBase}.url": getWebDriverUrl()
        ]
    }
}
