# Gradle Background Execute Plugin
This plugin is used for starting (and stopping) background items from within any of your 
tasks.  This can be a process or command, other tasks, or even closures.

The Selenium Gradle plugin uses this to start and stop a Selenium server
instance.

## Setup
Follow the [instructions on the Gradle Plugin page for this plugin][gradle-plugin-page].

## Usage
Once the plugin has been [applied][gradle-plugin-page], you can add a spawn closure to any 
task as follows:
```groovy
task myCustomTask() {
    spawn {
        // Put your custom spawn actions here
    }
}
```
Alternatively, you can add a spawn closure to an existing task by calling its spawn function:
```groovy
test.spawn {
    // Put your custom spawn actions here
}
```

Once you have a `spawn {}` closure defined, you can define as many spawn types in it as you 
want.  Each spawn type will be executed in the order specified.

There are 3 spawn types:
- [Process/Command](README_SPAWN.md#spawning-a-processcommand)
- [Task](README_SPAWN.md#spawning-a-task)
- [Closure](README_SPAWN.md#spawning-a-closure)

### Spawning a Process/Command
You can spawn a process/command using a `process {}` closure as follows:
```groovy
spawn {
    process {
        // Here we define the command to be run
        command = ['start-background','Parameter 1', 'Parameter 2', ...]

        // When this string is found in the output of the process,
        // the process gets backgrounded.
        ready = 'String to look for' 

        // This optional value is used to specify the directory from 
        // which the process will be spawned.  It defaults to the 
        // project's base directory.
        directory = project.projectDir
    }
}
```

### Spawning a Task
You can spawn a task using a `task {}` closure as follows:
```groovy
spawn {
    task {
        // Optional: Define the start task.  The task specified here
        // will run first but must then terminate before the current
        // task will run.
        start = project.tasks.runMeFirst

        // Optional: Define the end task.  The task specified here
        // will run last but must then terminate before the current
        // task will run.
        end = project.tasks.runMeLast
    }
}
```

### Spawning a Closure
You can spawn with a closure using a `closure {}` closure as follows:
```groovy
spawn {
    closure {
        // Optional: Define the start closure.  The code specified here
        // will run first but must then terminate before the current
        // task will run.
        start {
            // Specify any Groovy code you want to run before this
            // task starts.
        }

        // Optional: Define the end closure.  The code specified here
        // will run last.
        end {
            // Specify any Groovy code you want to run after this
            // task starts.
        }
    }
}
```


[gradle-plugin-page]: https://plugins.gradle.org/plugin/com.pledgecomputers.gradle.spawn